<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . '/core/API_core.php';

class Erp extends API_core {

    private $parent = 'erp';

    function __construct() {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');
    }

    function setting_post($type = NULL) {
        $respon = array('params' => $this->post());
        $allow_action = array(
            'new', 'update', 'detail', 'gets'
        );
        $params = $this->post();
        $action = $this->post('action');
        if (!in_array($action, $allow_action)) {
            log_add('no action action:' . $action);
            $this->result(FALSE, 'NOACT', "Action Unknown");
        } else {
            $respon[] = driver_run($this->parent, 'setting_' . $type, $type . '_' . $action, $params);
        }
        $this->result($respon);
    }

}
