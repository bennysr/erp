<?php

//api_helper
if (!function_exists('batch_email')) {

    function batch_email($to = "", $title = "", $message = "", $from = "demo@demo.com", $type = 'consultant') {
		//on progress
    }

}

if (!function_exists('driver_run')) {

    function driver_run($driver_core, $driver_name, $func_name = 'executed', $params = array()) {
        log_local("driver_run |$driver_core, $driver_name, $func_name");
        $result = array('code' => 0, 'data' => false, 'messages' => '');
        /* no drivers core =============================== */
        $core_file = ucfirst(strtolower($driver_core));
        if (!is_file(APPPATH . 'libraries/' . $core_file . '/' . $core_file . ".php")) {
            $result['messages'] = !is_local() ? 'no core driver file' : 'buatlah core drivernya di:' . APPPATH . 'libraries/' . $core_file . '/' . $core_file . ".php";
            $result['error'] = 100;
            log_local($result, 'driver_run');
            return $result;
        }

        $CI = & get_instance();
        log_local("run driver: $driver_core| $driver_name| $func_name");
        log_local("parameter:".count($params));
        /* 	Kita butuh file config khusus untuk daftar driver  */
        $config_file = 'driver_erp';
        if (!is_file(APPPATH . 'config/' . $config_file . ".php")) {
            log_local('buatlah confignya di:'.APPPATH.'config/'.$config_file.".php",'error');
            $result['messages'] = !is_local() ? 'no config file' : 'buatlah confignya di:' . APPPATH . 'config/' . $config_file . ".php\nbuatlah array confignya \$config['drivers_{$driver_core}']=array('{$driver_name}');";
            $result['error'] = 101;
            log_local($result, 'driver_run');
            return $result;
        }

        /* 	Kita butuh config parameter untuk daftar driver  */
        //$CI->config->load($config_file);
        //$valid_drivers = $CI->config->item('drivers_' . $driver_core);
        $valid_drivers = config_load('drivers_' . $driver_core, $config_file);
        if (is_null($valid_drivers) || $valid_drivers === false) {
            log_local("buatlah array confignya \$config['drivers_{$driver_core}']=array();",'error');
            $result['error'] = 102;
            $result['messages'] = !is_local() ? 'no config' : "buatlah array confignya \$config['drivers_{$driver_core}']=array();";
            log_local($result, 'driver_run');
            return $result;
        }
        /* 	Kita butuh nilai parameter yang sesuai untuk daftar driver  */
        log_local('exist?' . $driver_name);
        if (!in_array($driver_name, $valid_drivers)) {
            log_local("buatlah nilai '{$driver_name}' pada array confignya \$config['drivers_{$driver_core}']=array('{$driver_name}');",'error');
            $result['error'] = 103;
            $result['messages'] = !is_local() ? 'no config' : "buatlah nilai '{$driver_name}' pada array confignya \$config['drivers_{$driver_core}']=array('{$driver_name}');";
            log_local($result, 'driver_run');
            return $result;
        }

        /* keberadaan file driver ====================================  */
        $core_file = ucfirst(strtolower($driver_core));
        $driver_file = ucfirst(strtolower($driver_core)) . "_" . strtolower($driver_name);
        $file_check = APPPATH . 'libraries' . DIRECTORY_SEPARATOR . $core_file . DIRECTORY_SEPARATOR . 'drivers' . DIRECTORY_SEPARATOR . $driver_file . ".php";
        if (!is_file($file_check)) {
            log_local('buatlah file drivernya di:'.APPPATH.'libraries/'.$core_file.'/drivers/'.$driver_file.".php",'error');
            $result['error'] = 104;
            $result['messages'] = !is_local() ? 'no config' : 'buatlah file drivernya di:' . $file_check;
            log_local($result, 'driver_run');
			
			
            if (is_local()) {
                $target = APPPATH . 'libraries/' . $core_file . '/drivers/' . $driver_file . ".php";
                $str = '';
                $oth = array(
                    'new', 'update', 'detail', 'gets', 'select', 'total'
                );
                for ($i = 0; $i < count($oth); $i++) {
                    $dshorname = substr(trim($driver_name), 0, 2);
                    $str .= "\n
    /**
     * Fungsi : {$dshorname}_{$oth[$i]}
     *
     * Kegunaan ketik disini
     *
     * @param	type	\$params 	array
     * 
     * @return	void
     */
\tpublic function {$dshorname}_{$oth[$i]}(\$params=FALSE) {\n
\t\t\$result	=	array();
\n\n\t\treturn \$result;		
\t}";
                }

                $scripts = "<?php\n//========= $target \n
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of {$driver_file}
 * {$driver_core} {$driver_name}
 * @author Auto Generate
 */
class {$driver_file} extends CI_Driver{\n
    private \$urls, \$privatekey, \$db_main;
    public \$CI;
\tfunction __construct() {
        \$CI = & get_instance();
        //\$this->db_main = \$CI->base_model->db_main;
        //\$CI->load->model('mujur/mujur_settings_db');
         if (is_local()) { 
            log_add('{$driver_core}| {$driver_name}', 'library');
        }
\t}\n
    /**
     * Fungsi : {$func_name}
     *
     * auto generate on " . date("Y-m-d H:i:s") . "
     *
     * @param	type	\$params 	array
     * 
     * @return	void
     */
\tpublic function {$func_name}(\$params=FALSE) {
\t\t\$result	=	array();
\n\n\t\treturn \$result;		
\t}\n$str\n
/*     * MORE* */
\n} ";
                $scripts = str_replace("\t", "    ", $scripts);
                $status = file_put_contents($target, str_clean($scripts), FILE_APPEND);
                $result['auto_create'] = $status;
                $result['message2'] = 'refresh again';
            }
			
            return $result;
        }
        log_local('file OK:' . $driver_file);

        /* 	Kita butuh functionnya ==================================  

          $result['data']=$CI->{$driver_core}->{$driver_name}->{$func_name}($params);
         */
        log_local('run driver core:?' . $driver_core);
        $CI->load->driver($driver_core);
		
        if (!method_exists($CI->{$driver_core}->{$driver_name}, $func_name)) {
            /*
             * in_array() expects parameter 2 to be array, null given
             */
            log_local('buatlah fungsi '.$func_name.'($params) pada file drivernya di:'.APPPATH.'libraries/'.$core_file.'/drivers/'.$driver_file.".php",'error');
            $result['error'] = 105;
            $result['messages'] = !is_local() ? 'no config' : 'buatlah fungsi ' . $func_name . '($params) pada file drivernya di:' . APPPATH . 'libraries/' . $core_file . '/drivers/' . $driver_file . ".php";
            log_local($result, 'driver_run');
            return $result;
        } else {
            $result['error'] = false;
            $result['messages'] = 'success';
			log_local('run driver:?' . $driver_name);
            $result['data'] = $CI->{$driver_core}->{$driver_name}->{$func_name}($params);
            log_local($result, 'driver_run');
        }

        return isset($result['data']) ? $result['data'] : array();
    }

}


if (!function_exists('_runApi')) {

    function _runApi($url, $parameter = FALSE, $debug = FALSE) {
        global $maxTime;
        $times = array(microtime());
        $times['begin'] = date("Y-m-d H:i:s");
        $user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13';
        if ($maxTime == null) {
            $maxTime = 10;
        }

        if (isset($parameter['maxTime'])) {
            $maxTime = $parameter['maxTime'];
            unset($parameter['maxTime']);
        }

        $CI = & get_instance();
        $times['start'] = microtime();
        $dtAPI = array('url' => $url);

        if (count($parameter) && $parameter !== FALSE) {
            $logTxt = "func:_runApi| url:{$url}| param:" . http_build_query($parameter, '', '&');
        } else {
            $logTxt = "func:_runApi| url:{$url}";
            //    $parameter['info'] = 'no post';
        }

        $times[] = microtime();
        $logTxt .= "| max time:{$maxTime}";
        //$parameter[]=array('server'=>$_SERVER);
        $dtAPI['parameter'] = json_encode($parameter);
        log_add('API: ' . $logTxt, 'log');

        $curl = curl_init();
        $times['init'] = microtime();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $times['set opt1'] = microtime();

        if (count($parameter) && $parameter !== FALSE) {
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_TIMEOUT, $maxTime);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($parameter, '', '&'));
            $times['set opt2'] = microtime();

            //curl_setopt($ch, CURLOPT_POSTFIELDS, array('file' => '@/path/to/file.ext');
            //if( isset($_SERVER['HTTP_USER_AGENT']) )
            //	curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);

            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
            $times['set opt3'] = microtime();

            log_add('API (POST): ' . "url:{$url}| param:\n" . print_r($parameter, 1), 'log');
        } else {
            log_add('API (GET): param:' . print_r(parse_url($url), 1), 'log');
        }

        curl_setopt($curl, CURLOPT_USERAGENT, $user_agent);
        $times['set useragent'] = microtime();

        $raw_response = $response = curl_exec($curl);
        $times['execute'] = microtime();

        if (0 != curl_errno($curl)) {
            $response = new stdclass();
            $response->code = '500';
            $response->message = curl_error($curl);
            $response->maxTime = $maxTime;
            $times['error 1'] = microtime();
        } else {
            $response0 = $response;

            $response = json_decode($response, 1);
            if (!is_array($response)) {
                $response = $response0;
                $times['error 2'] = microtime();
            } else {
                log_add('API not array');
            }
        }

        curl_close($curl);
        $times['close'] = microtime();

        if (!isset($response0)) {
            $response0 = '?';
        }

        logCreate('helper| API| url:' . $url . "|raw:" . (is_array($response) ? 'total array/obj=' . count($response) : $response0 ));

        $times['end'] = date("Y-m-d H:i:s");

        if ($debug) {
            $response = array(
                'parameters' => $parameter,
                'url' => $url,
                'time' => $times,
                'response' => $response,
                'raw' => $raw_response
            );
        }

        return $response;
    }

}
 

