<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 */

function core_folder() {
    $folder = config_load('logs_win');
    if ($folder)
        return '';

    $ar = explode("/", $_SERVER['SCRIPT_FILENAME']);
    unset($ar[count($ar) - 1]);
    $doc_root = implode("/", $ar) . "/";
    return $doc_root;
}

if (!function_exists('config_loads')) {

    function config_load($name = 'test', $header = FALSE) {
        $CI = & get_instance();
        if ($header) {
            $CI->config->load($header, TRUE);
            $value_config = $CI->config->item($name, $header);
        } else {
            $value_config = $CI->config->item($name);
        }
        //print_r($value_config);
        return $value_config;
    }

}

function echo_r($arr) {
    echo is_local() ? '<pre>' . print_r($arr, 1) . '</pre>' : FALSE;
}

function js_goto($url, $time = 0) {
    if ($time == 0) {
        echo "<script>window.location.href = '{$url}';</script>";
    } else {
        $time_miliseconds = $time * 1000;
        echo '<script>
		setTimeout(function(){
    window.location.href = "' . $url . '";
}, ' . $time_miliseconds . ');</script>';
    }
    die;
}

function js_new_window($url) {
    echo "<script>window.open('{$url}');</script>";
}

function lock_type($type = false) {
    if ($type === false)
        return false;
    if (is_file('lock/' . $type . '.txt')) {
        return true;
    }
    return false;
}

function is_local() {
    $return = config_site('local');
    return $return;
}

function main_query($sql) {
    $CI = & get_instance();
    $main_db = $CI->load->database('main', true);
    log_config($sql, 'query');
    return $main_db->query($sql);
}

if (!function_exists('myIP')) {

    function myIP() {
        $ip = 'unknown';
        if (isset($_SERVER['HTTP_CLIENT_IP']) && !empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && !empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null;
        }
        return $ip;
    }

}

if (!function_exists('mySiteUrl')) {

    function mySiteUrl() {
        $CI = & get_instance();
        $ar = array();
        for ($i = 1; $i < 10; $i++) {
            $url = $CI->uri->segment($i);
            if ($url != '') {
                $ar[] = $url;
            }
        }
        //$this->uri->segment(2);
        return implode('/', $ar);
    }

}

if (!function_exists('show_alert_message')) {

    function show_alert_message($alert) {
        $CI = & get_instance();

        return true;
    }

}

function config_site($name, $config_file = 'forex_config') {
    $CI = & get_instance();
    if ($config_file != 'forex_config') {
        $CI->config->load($config_file, TRUE);
        $config_value = $CI->config->item($name, $config_file);
    } else {
        $config_value = $CI->config->item($name);
    }


    return $config_value;
}

function db_id($name = 'id', $sub = NULL, $detail = NULL, $min_num = 1000000) {
    $CI = & get_instance();
    $table = 'z_id';
    if (!$CI->db->table_exists($table)) {
        $CI->load->dbforge();
        $fields = array(
            'num' => array(
                'type' => 'BIGINT',
                'auto_increment' => TRUE
            ),
            'modified' => array(
                'type' => 'timestamp',
            )
        );
        $CI->dbforge->add_field($fields);
        $CI->dbforge->add_key('num', TRUE);
        $attributes = array('ENGINE' => 'myisam');
        $CI->dbforge->create_table($table, TRUE, $attributes);
    } else {
        $now = date("Y-m-d H:i:s", strtotime('-12 hours'));
        $sql = "delete from $table where modified<'$now'";
        $CI->db->query($sql);
    }
    $aSql = array();
    if (!$CI->db->field_exists('nama', $table)) {
        $aSql[] = "ALTER TABLE `{$table}` ADD `nama` char(255) NULL";
    }
    if (!$CI->db->field_exists('sub', $table)) {
        $aSql[] = "ALTER TABLE `{$table}` ADD `sub` char(255) NULL";
    }
    if (!$CI->db->field_exists('detail', $table)) {
        $aSql[] = "ALTER TABLE `{$table}` ADD `detail` text NULL";
    }

    foreach ($aSql as $sql) {
        $CI->db->query($sql);
    }

    $data = array(
        'nama' => $name,
        'sub' => $sub,
        'detail' => $detail
    );
    $CI->db->insert($table, $data);
    $num = $CI->db->insert_id();
    if ($num < $min_num) {
        $num = $min_num;
        $data['num'] = $num;
        $CI->db->insert($table, $data);
    }

    return $num;
}

if (!function_exists('rand_word')) {

    function rand_word($n = 5) {
        $tmp = "1234567890poiuytrewqasdfghjklzxcvbnm";
        $word = strtoupper($tmp) . strtolower($tmp);
        $str = '';
        for ($i = 0; $i < $n; $i++) {
            $str .= substr($word, rand(0, strlen($word)), 1);
        }
        return $str;
    }

}

/**
 * Fungsi : str_clean
 *
 * Kegunaan ketik disini 
 * 
 * @return	void
 */
if (!function_exists('str_clean')) {

    function str_clean($str) {
        $ar = explode("\n", $str);
        $s = '';
        foreach ($ar as $v) {
            $s .= rtrim($v) . "\n";
        }
        return $s;
    }

}

if (!function_exists('params_exist')) {

    function params_exist($params, $key, $default = FALSE) {
        if (isset($params[$key])) {
            return $params[$key];
        }
        if (is_array($params)) {
            log_local('params_exist key (' . $key . ' ?):' . json_encode(array_keys($params)));
        } else {
            log_local('params_exist (' . $key . ' ?) key:' . print_r($params, 1));
        }
        return $default;
    }

}