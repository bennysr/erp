<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Settings_table
 *
 * @author R700
 */
class Logs_table extends CI_Model {

    public $table;
    public $db_main;
    public $field_id;

    public function __construct() {
        parent::__construct();
        //$log_allow = config_load('logs');
        $this->table = 'mujur';
        $this->field_id = 'l_id';
        $this->db_main = $this->load->database('logs', TRUE);

        $data = $this->created_table();
        $data2 = $this->created_table('_back');
        /*
          2x pembuatan table satunya table history
         */
    }

    public function tablename() {
        return $this->db_main->dbprefix($this->table);
    }

    function created_table($etc = '') {
        if (ENVIRONMENT != 'development')
            return TRUE;
        if ($this->table === FALSE)
            return FALSE;
        if ($this->table == "")
            return FALSE;

        $table = $this->table . $etc;
        $table_name = $this->db_main->dbprefix($table);
        if (!$this->db_main->table_exists($table)) {
            $forge = $this->load->dbforge($this->db_main, TRUE);
            //------------
            $fields = array(
                $this->field_id => array(
                    'type' => 'BIGINT',
                    'auto_increment' => TRUE
                ),
                'modified' => array(
                    'type' => 'timestamp',
                ),
                'deleted' => array(
                    'type' => 'tinyint',
                    'default' => 0,
                ),
            );

            $forge->add_field($fields);
            $forge->add_key($this->field_id, TRUE);
            $attributes = array('ENGINE' => 'myisam');
            $forge->create_table($table, TRUE, $attributes);
        }

        //===========field exists
        $aSql = array();
        if (!$this->db_main->field_exists('type', $table)) {
            $aSql[] = "ALTER TABLE `{$table_name}` ADD `type` char(50) NULL 
			COMMENT 'kode random', ADD INDEX (`type`)";
        }
        if (!$this->db_main->field_exists('debug', $table)) {
            $aSql[] = "ALTER TABLE `{$table_name}` ADD `debug` char(50) NULL  ";
        }
        if (!$this->db_main->field_exists('detail', $table)) {
            $aSql[] = "ALTER TABLE `{$table_name}` ADD `detail` longtext";
        }

        foreach ($aSql as $sql) {
            $this->db_main->query($sql);
            if (is_local()) {
                log_add('sql:' . $sql, 'table');
            }
        }
    }

}
