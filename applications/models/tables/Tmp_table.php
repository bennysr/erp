<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Settings_table
 *
 * @author R700
 */
class Tmp_table extends CI_Model {

    public $table;
    public $db_main;
    public $field_id;

    public function __construct() {
        parent::__construct();
        $log_allow = config_load('logs');
        $this->table = 'z_tmp';
        $this->field_id = 'l_id';
        $this->db_main = $this->load->database('main', TRUE);
    }

    public function tablename() {
        return $this->db_main->dbprefix($this->table);
    }

}
