<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Mars_theme_core
 *
 * @author R700
 * CI_Controller
 */
class Mars_theme extends CI_Controller {

    //put your code here
    protected $params;
    private $default_theme, $config = FALSE,$done=FALSE;

    public function __CONSTRUCT() {
        parent::__construct();
        $CI = & get_instance();
        log_add('Mars_theme START (1)');
        $this->default_theme = 'mars_tmp';

        //log_add( 'mars_theme load');
        $this->controllers_okay();
        log_add('controller Okay end');
        $this->config = FALSE;
        db_id('mars', 'theme');
        $this->base_model->log_backup();
    }

    function __DESTRUCT() {
        log_add('desctruct start');
        
        if(!$this->done){
            $this->execute_view();
        }
        log_add('done');
    }
    
    function execute_view(){
        $CI = & get_instance();
        $this->controllers_okay();
        if (!isset($this->params['stop'])) {
            $uri = array();
            for ($i = 1; $i < 5; $i++) {
                $uri[$i] = $CI->uri->segment($i, "index");
            }

            log_add('uri:' . json_encode($uri), 'debug', 'core');

            if (!$this->config) {
                log_add('no config?');
                $header = $uri[1] . "/" . $uri[2];
                $page = $uri[3];
                //4..5 biasanya id atau lainnya
                $this->config($header, $page);
                log_add('load config default:'.$header." |page:".$page);
            }

            echo $this->load->view($this->default_theme, $this->params, TRUE);
            //echo $txt;
        }
        log_add('Mars_theme END');
        $this->done=TRUE;
    }

    function config($header, $page = 'index') {
        $CI = & get_instance();
        log_add('config START :' . json_encode($header) . "|" . json_encode($page));

        if ($this->config == TRUE) {
            log_add('load OK.. Pass this');
            return TRUE;
        }

        $config_file = APPPATH . "config" . DIRECTORY_SEPARATOR . $header . "_config.php";
        if (is_file($config_file)) {
            $configs = config_load($page, $header . "_config");
            $this->config = TRUE;
        } else {
            $configs = NULL;
        }

        if ($configs == NULL) {
            log_add('load NOT OK.. use DEFAULT');
            $configs = config_load('basic_controller');
            $configs['config_file'] = $config_file;
        }

        if (is_array($configs)) {
            log_add('load OK.. header:' . count($configs));
            foreach ($configs as $key => $value) {
                $this->params[$key] = $value;
            }
        }

        $params = array_keys($this->params);
        //log_message('debug', 'params:' . json_encode($params));
        log_add('config| start params:' . json_encode($params), 'debug', 'core');
//====================content logic
        $load_content = $this->uri->segment(4);
        if (isset($this->params['contents'])) {
            log_add('contents params:' . json_encode($this->params['contents']), 'debug', 'core');
            $content = $this->params['contents'];
            if (isset($content[$load_content])) {
                $this->params['content'] = $content[$load_content];
            } else {
                $this->params['content'] = $content[0];
            }
            log_add('content load:' . $load_content . " res:" . json_encode($this->params['content']), 'debug', 'core');
        }

        $params = array_keys($this->params);
        log_add('config end params:' . json_encode($params), 'debug', 'core');
        log_add('config END :' . json_encode($header) . "|" . json_encode($page));
        return TRUE;
    }

    public function controllers_okay() {
        if (!is_local())
            return TRUE;
        //require APPPATH . "core" . DIRECTORY_SEPARATOR . "Mars_theme_core.php";
        $site1 = $this->uri->segment(1);
        $site2 = $this->uri->segment(2);
        $file_controllers = $site1 . '/' . ucfirst($site2) . '.php';
        /*
          if(!is_file(APPPATH . "config" . DIRECTORY_SEPARATOR.$file_controllers)){
          logCreate('error:no controllers|'.$file_controllers); die('not valid?');
          }
         *
         */
        $list_folder = array(
            'views/mars/contents',
            'views/mars/datatables',
            'views/mars/forms',
            'views/mars/widgets',
        );
        foreach ($list_folder as $folder) {
            $folder_check = APPPATH . $folder . DIRECTORY_SEPARATOR . $site2;
            if (!is_dir($folder_check)) {
                mkdir($folder_check);
            } else {
                log_add($folder_check . " OK");
            }
        }
    }

    public function stop() {
        $this->params['stop'] = 1;
    }

}
