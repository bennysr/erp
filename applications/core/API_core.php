<?php

//===========extend core REST=========
require_once APPPATH . '/libraries/REST_Controller.php';
date_default_timezone_set('Asia/Jakarta');

class API_core extends REST_Controller {

    function __construct() {
        parent::__construct();
        ob_start();
        header('Access-Control-Allow-Origin: *');
    }

    protected function require_post($post_header) {
        $post = $this->post();
        $valid = array('status' => TRUE, 'require' => array());
        foreach ($post_header as $head) {
            if (!isset($post[$head])) {
                $valid['status'] = FALSE;
                $valid['require'][] = $head;
            }
        }

        if ($valid['status'] !== TRUE) {
            $respon['code'] = 'NOPARAM';
            $respon['message'] = 'Parameter Require';
            log_add('user/login |error:' . json_encode($valid), 'api');

            $respon=array();
            if(ENVIRONMENT=='development'){
                $respon['missing']=$valid['require'];
                $respon['require']=$post_header;
				$respon['post']=$post;
				
            }
            $this->result($respon, 'NOPARAM', 'PARAMETER MISSING');

        }

        return $valid;
    }

    protected function check_token() {
        $token = $this->post('token');
        $params = array(
            'field' => 'token',
            'value' => $token
        );
        $result = driver_run('gundam', $driver_name = 'token', $func_name = 'get_id', $params);

        //echo "error";
        //$row=$this->token_db->_get_id($token);
        if ($result !== TRUE) {
            $respon['code'] = 99;
            $respon['message'] = 'Token Invalid';
            $respon['data'] = $result;
            log_add('user/login |error:' . json_encode($result), 'api');
            $this->result($result, 'NOTOKEN', "Token invalid");
        }
    }

    protected function result($data = array(), $code = 200, $message = 'success') {
        log_local($data, 'result');
        $error = ob_get_contents();
        ob_flush();
        $respon = array(
            'code' => (string) $code,
            'message' => $message,
            'data' => $data
        );

        if ($error != '') {
            $respon['debug_error'] = explode("\n", $error);
        }

        ob_end_flush();
        $this->response($respon, 200);
    }
 
    protected function app_login(){
        $header = array( 'app_id','secret_code');
        $app_id=$this->post('app_id');
        $secret_code=$this->post('secret_code');
        $this->require_post($header);
        $app=$app_detail=FALSE;
        $types=config_load('type','app_client');
        foreach($types as $type){
            $keys=config_load($type ,'app_client');
            if(isset($keys[$app_id])){
                $app=$type;
                $app_detail=$keys[$app_id];
            }
        }
        
        if(!$app_detail || !$app){
            $this->result(FALSE,'UAC','Config Unknown');
        }
        
        if(!isset($app_detail['secret_code'])){
            $this->result(FALSE,'USC','Config Unknown');
        }
        
        if( $app_detail['secret_code'] != $secret_code){
            $this->result(FALSE,'ISC','Config Unknown');
        }
        
        return $app;
    }
 
	protected function allowed_client(){
		return $this->app_login();
		$headers=array(
			'client_id','private_key'
		);
		$this->require_post($headers);
		
		$client_id=$this->post('client_id');
		$private_key=$this->post('private_key');
		$result=array_keys($this->post());
		//==========Tarik dari config======
		$configs=config_load("client","api_client");
		
		$result['client']=array(
					rand_word(11),
					rand_word(32)
				);
				
		if(isset($configs[$client_id])){
			$config=$configs[$client_id];
		}else{
			$this->result($result, 'CIU', "Please Check Your Config");
		}
		
		if($config!=$private_key){
			$this->result($result, 'PKU', "Please Check Your Config");
		}
			
		
		return TRUE;
	}
 
}
