<?php
//========= D:\php\erp\application\libraries/Erp/drivers/Erp_setting_category.php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Erp_setting_category
 * erp setting_category
 * @author Auto Generate
 */
class Erp_setting_category extends CI_Driver{

    private $urls, $privatekey, $db_main;
    public $CI;
    function __construct() {
        $CI = & get_instance();
		$CI->load->model('mujur/mujur_settings_db');
		log_local('erp_setting');
    }

    /**
     * Fungsi : category_gets
     *
     * auto generate on 2018-06-30 23:45:55
     *
     * @param    type    $params     array
     *
     * @return    void
     */
    public function category_gets($params=FALSE) {
        $CI = & get_instance();
        if (is_local()) {
            log_add('cat_get params ' . json_encode($params), 'db');
        }

        $limit = isset($params['limit']) ? $params['limit'] : 2;
        $start = isset($params['start']) ? $params['start'] : 0;
		$filter = isset($params['filters']) ? $params['filters']:array();
        $detail = $CI->mujur_settings_db->cat_gets($filter , $limit, $start)  ;
        $total =  $CI->mujur_settings_db->cat_gets($filter, 1, 0, TRUE)  ;
        $result = array(
            'detail' => $detail,
            'total' => $total,
            'params' => $params
        );
        $result['raw'] = isset($params['filters']) ? $CI->mujur_settings_db
                ->cat_gets($params['filters'], $limit, $start,FALSE,TRUE) : FALSE;

        return $result;
    }
 
/*     * MORE* */

}
