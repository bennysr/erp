<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
$active_group = 'default';
$query_builder = TRUE;
$db['default'] = array(
	'dsn'	=> '',
	'hostname' => '127.0.0.1',
	'username' => 'valas',
	'password' => '',
	'database' => 'valas',
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);


$db['main'] = array(
	'dsn'	=> '',
	'hostname' => '127.0.0.1',
	'username' => 'valas',
	'password' => '',
	'database' => 'valas',
	'dbdriver' => 'mysqli',
	'dbprefix' => 'v4t_',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE

);

$db['logs'] = array(
	'dsn'	=> '',
	'hostname' => '127.0.0.1',
	'username' => 'valas',
	'password' => '',
	'database' => 'logs',
	'dbdriver' => 'mysqli',
	'dbprefix' => 'z_',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);

$db['salma']['hostname'] = 'localhost';
$db['salma']['username'] = 'mujur_forex';
$db['salma']['password'] = 'v5aKFPRKDFxhV6A4';
$db['salma']['database'] = 'mujur_forex';
$db['salma']['dbdriver'] = 'mysqli';
$db['salma']['dbprefix'] = ''; //NO PREFIX
$db['salma']['pconnect'] = FALSE;
$db['salma']['db_debug'] = TRUE;
$db['salma']['cache_on'] = FALSE;
$db['salma']['cachedir'] = '';
$db['salma']['char_set'] = 'utf8';
$db['salma']['dbcollat'] = 'utf8_general_ci';
$db['salma']['swap_pre'] = '';
$db['salma']['autoinit'] = TRUE;
$db['salma']['stricton'] = FALSE;